Horizon Vault 2.0
==============

## Project Overview

This Git repository houses a set of components that uses the React library (http://facebook.github.io/react/) for defining the user interface of a television-based UI.  This code is being used as a proof of concept to show React based components running in HTML on a Raspberry Pi B+ embedded system.

### CCA
This repository is under CCA, as is the code for the application. **DO NOT SHARE THIS CODE WITH ANYONE!**

### Dependencies

The application is tested with the following stack:
* Google Chrome latest version (http://chrome.google.com)
* Raspberry Pi B+ (http://www.raspberrypi.org/products/model-b-plus/)
* node.js 0.12.0 (https://nodejs.org)
* npm (Bundled with Node.js)
* gulp 3.8.11 (http://gulpjs.com)
* Dependencies (Managed using npm)
 * del
 * gulp
 * gulp-react
 * gulp-webpack
 * gulp-connect
 * jsx-loader
 * react
 * react-tools
 * stats-monitor
 * webpack

### Configuration

The examples (demos) will be served using an embedded web server, and the default port is 8080 (specified in gulpfile.js). You may change the to anther port by changing the environment variable ```process.env.PORT```.

##### Performance Logger

<WebGL> react component has a property "logEnabled" which can be used to turn on/off performance logger, and the performance logger is turned off by default. You may turn it on by setting the "logEnabled" property as true. Also the optional "logMode" property can be used to choose the default log mode (0 for FPS mode, 1 for rendering time mode).
	
##### Performance Test Parameters

The performance test parameters are specified in examples/performance/app.js, and you may change the parameters to tune the performance test.  Specifically, the UPDATE_INTERVAL, MAX, and STEP values can be updated.


### Building the code

* Open terminal (command line), and go to the cloned folder from Git.
* Install gulp globally (use "sudo" if needed on OS X or Linux systems): ```npm install -g gulp```
* Install dependencies and build the library ```npm install```
* Build and serve the examples in a local web server: ```gulp```

Once the build finishes, you will see message "Webpack is watching for changes" as follows, and now the examples are hosted on your local machine at http://localhost:8080/examples
 


## Raspberry Pi

You can find a system image that can be installed on an SD card here: https://www.dropbox.com/s/wq2k73skd5tqwju/pi_image.img.bz2?dl=0

Detailed installation instructions are here:  https://www.dropbox.com/s/2zdq6x9plq3b5nc/RaspberryPiInstallationInstructions.pdf?dl=0

To use, you can just run the gulp server on your development computer, SSH into the Raspberry Pi and run a command to open one of the examples on your development computer.  As an example:

```qtbrowser --url=http://192.168.2.24:8080/examples/test-ui/index.html```