/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This is a simple demonstration of the <WebGL> component.
 *
 * @version 1.0
 * @author albertwang
 */

'use strict';

var React = require('react');
var ReactWebGL = require('react-webgl');
var WebGL = ReactWebGL.WebGL;
var Quad = ReactWebGL.Quad;

var App = React.createClass({
    render: function() {
        return (
            <WebGL logEnabled={true} logMode={0} width={1024} height={768}>
                <Quad x={100} y={10} width={150} height={150} backgroundColor="rgba(255, 0, 54, 1)" zIndex={-1}>
                </Quad>
            </WebGL>
        )
    }
});

React.render(<App/>, document.body);
