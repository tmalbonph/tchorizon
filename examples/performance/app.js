/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This is a performance demonstration of the <WebGL> and <Quad> components.
 *
 * @version 1.0
 * @author albertwang
 */

'use strict';

var React = require('react');
var ReactUpdates = require('react/lib/ReactUpdates');

var ReactWebGL = require('react-webgl');
var WebGL = ReactWebGL.WebGL;
var Quad = ReactWebGL.Quad;

/**
 * Represents the interval (in milliseconds) to append <Quad> components to the canvas.
 * @type {number}
 */
var UPDATE_INTERVAL = 1000;

/**
 * Represents the maximum number of <Quad> components to add.
 * @type {number}
 */
var MAX = 1000;

/**
 * Represents the number of <Quad> components to add in each round.
 * @type {number}
 */
var STEP = 100;

/**
 * Generate random integer in range.
 * @param min the minimum value
 * @param max the maximum value
 * @returns the random integer
 */
function randomInteger(min, max) {
    return parseInt((Math.random() * (max - min + 1)), 10) + min;
}

/**
 * Represents the counter of added <Quad> components.
 * @type {number}
 */
var counter = 0;

// Create the App component
var App = React.createClass({
    /**
     * Render the component.
     * @returns the component
     */
    render: function() {
        return (
            <WebGL ref="webGL" logEnabled={true}>
                {this.props.children}
            </WebGL>
        );
    },

    /**
     * Set up scheduled task to dynamically add <Quad> elements to the <WebGL> element.
     */
    componentDidMount: function() {
        var webGL = this.refs.webGL;
        var quadElements = [];
        var task = setInterval(function() {
            var x, y, width, height, backgroundColor, zIndex, quad;
            // As we add children dynamically, we need to use ReactUpdates.ReactReconcileTransaction to
            // reconcile changes
            var transaction = ReactUpdates.ReactReconcileTransaction.getPooled();
            for (var i = 0; i < STEP; i++) {
                // Generate the color
                backgroundColor = 'rgba(' + randomInteger(0, 254) + ',' + randomInteger(0, 254)
                    + ',' + randomInteger(0, 254) + ',' + (i % 2 == 0 ? 1 : Math.random()) + ')';

                zIndex = randomInteger(0, STEP);
                if (i % 2 == 0) {
                    // Add to root WebGL element
                    x = randomInteger(0, webGL.props.width / 2);
                    y = randomInteger(0, webGL.props.height / 2);
                    width = randomInteger(0, webGL.props.width / 2);
                    height = randomInteger(0, webGL.props.height / 2);
                    quad = <Quad x={x} y = {y} width={width} height={height}
                        backgroundColor={backgroundColor} zIndex={zIndex} />;
                    transaction.perform(
                        webGL.mountAndInjectChildrenAtRoot,
                        webGL,
                        [quad],
                        transaction
                    );
                } else {
                    // Add to an existing Quad element
                    var parentQuad = quadElements[randomInteger(0, quadElements.length - 1)];
                    x = randomInteger(0, parentQuad.props.width / 2);
                    y = randomInteger(0, parentQuad.props.height / 2);
                    width = randomInteger(0, parentQuad.props.width / 2);
                    height = randomInteger(0, parentQuad.props.height / 2);
                    quad = (<Quad x={x} y = {y} width={width} height={height}
                        backgroundColor={backgroundColor} zIndex={zIndex} />);
                    transaction.perform(
                        webGL.mountAndInjectChildrenAtRoot,
                        parentQuad,
                        [quad],
                        transaction
                    );
                }
                quadElements.push(quad);
            }
            ReactUpdates.ReactReconcileTransaction.release(transaction);

            // Accumulate the counter and terminate the task once the MAX is reached
            counter += STEP;
            if (counter >= MAX) {
                clearInterval(task);
            }
        }, UPDATE_INTERVAL);
    }
});

React.render(<App/>, document.body);






