/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This is the Tween implementation.
 *
 * @providesModule Tween
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';

function Tween (targetObject, properties, duration) {
    var field, key;

    this._startValues = {};
    for (field in targetObject) {
        if (typeof (targetObject[field]) === 'number') {
            this._startValues[field] = targetObject[field];
        }
    }
    this._endValues = {};
    for (key in properties) {
        if (typeof properties[key] === 'number') {
            this._endValues[key] = properties[key];
        }
    }

    this._targetObject = targetObject;
    this._duration = duration;
    this._startTime = null;
};

Tween.prototype.start = function () {
    this._startTime = Date.now();
    return this;
};

/**
 * Set onupdate callback
 * @param callback
 * @returns {vod.Tween}
 */
Tween.prototype.onUpdate = function (callback) {
    this._onupdate = callback;
    return this;
};

/**
 * Set oncomplete callback
 * @param callback
 * @returns {vod.Tween}
 */
Tween.prototype.onComplete = function (callback) {
    this._oncomplete = callback;
    return this;
};

/**
 * Update tweening step
 * @param time Current time
 * @returns {boolean}
 */
Tween.prototype.update = function (time) {
    var t, t1, key, elapsed;
    if (time < this._startTime) {
        return false;
    }
    elapsed = (time - this._startTime) / this._duration;
    elapsed = elapsed > 1 ? 1 : elapsed;
    t = elapsed;
    t1 = 1 - t;
    for (key in this._endValues) {
        if (typeof this._startValues[key] === 'number') {
            this._targetObject[key] = this._startValues[key] * t1 + this._endValues[key] * t;
        }
    }
    if (this._onupdate !== undefined) {
        this._onupdate.call(this._targetObject, t);
    }
    if (elapsed === 1) {
        if (this._oncomplete !== undefined) {
            this._oncomplete.call(this._targetObject);
        }
        return true;
    }
    return false;
};

module.exports = Tween;